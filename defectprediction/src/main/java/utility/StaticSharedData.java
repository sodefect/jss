package utility;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.HashMap;

public class StaticSharedData {

    public static HashMap<String, Double> postScores = new HashMap<>();
    public static HashMap<String, Double> apiScores = new HashMap<>();
    public static final String POST_SCORES = "post_scores.csv";
    public static final String API_SCORES = "api_scores.csv";
    public static final String ENTROPIES = "entropies.txt";
    public static final String datasetBasePath = "D:\\Research\\Datasets\\";
    public static final String STORMED_DATASET_PATH = datasetBasePath+"stormed-dataset";
    public static final String PATH_TO_GIT_REPO = "/home/tahmooresi/projects/spring-petclinic/.git";
    public static final int POST_GENERAL_TOPIC_THRESHOLD = 2000;
    public static final String COMMIT_GURU_DIR = "/home/tahmooresi/commit-guru";


//    public static final String projectName = "AmazeFileManager";
    public static final String projectName = "elasticsearch";
    public static final String releaseCommitHash = "37cdac16ade656943c6518357df8464f686cb814";

    public static final String csvBaseDirectory = datasetBasePath+"CSVs"+ FileSystems.getDefault().getSeparator();
    public static final String discussionMetricsCSV = csvBaseDirectory+"discussion-metrics.csv";
    public static final String apiEntropiesCSV = csvBaseDirectory+"api-entropies.csv";
    public static final String stormedDatasetPath = datasetBasePath+"stormed-dataset";
    public static final String projectsGitDirectory = datasetBasePath+"repos"+FileSystems.getDefault().getSeparator();
    public static final String commitGuruDirectory = csvBaseDirectory + "commit.guru"+FileSystems.getDefault().getSeparator();
    public static final String projectsCommitApiDirectory = csvBaseDirectory + "commits-apis"+FileSystems.getDefault().getSeparator();
    public static final String apiPostPositionsCSV = csvBaseDirectory + "api-post-positions.csv";
    public static final String apiPostPositionsDigestCSV = csvBaseDirectory + "api-post-positions-digest.csv";
    public static final String resultsCSVDirectory = csvBaseDirectory + "results"+FileSystems.getDefault().getSeparator();


}
