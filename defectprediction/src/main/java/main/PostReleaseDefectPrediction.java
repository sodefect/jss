package main;

import main.utils.*;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class PostReleaseDefectPrediction extends Loggable{
    private APIChallengeCalculator acc;
    List<FileData> files = new ArrayList<>();
    private List<Feature> features;
    public PostReleaseDefectPrediction(APIChallengeCalculator acc){
        this.acc = acc;
    }
    public void preprocess() throws IOException {
        acc.preprocess();
    }
    public void loadFilesData(String FilesDataPath) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(FilesDataPath);
        for (CSVRecord csvRecord : csvParser) {
            FileData file = new FileData();
            file.SetData(csvRecord);
            files.add(file);
        }
    }
    public void computeFileChallenges(){
        for (FileData file : files){
            for (Feature feature: features) {
                calculateFeatureForFile(file, feature);
            }
        }
    }
    public void calculateFeatureForFile(FileData file, Feature feature) {
        Double apiChallenge = 0.0;
        List<String> fileApis = new ArrayList<String>();
        for (String api : file.getApis()){
            Double challenge = acc.getApiChallenge(api, feature);
            if (challenge == null) {
                continue;
            }
            fileApis.add(api + ":"+challenge);
            apiChallenge += challenge;
        }
        file.setApisWithScore(fileApis);
        apiChallenge /= file.getLoc();
        file.setFeature(feature, apiChallenge);

    }

    public void writeResultsToFile(String outputPath) throws IOException {
        log("Writing results to file");
        CSVWriter csvWriter = new CSVWriter();
        csvWriter.initializeCSVPrinter(outputPath, getHeader());
        int skippedFileCount = 0;
        for (FileData file : files){
            List<String> line = file.getData();
            boolean skipFile = false;
            for (Feature feature : file.getFeatures()) {
                Double featureValue = file.getFeature(feature);
                if (featureValue.isNaN() || featureValue.isInfinite()){
                    skipFile = true;
                }
                line.add(String.valueOf(featureValue));
            }
            if (skipFile){
                skippedFileCount ++;
                continue;
            }
            String[] arrayLine = new String[line.size()];
            csvWriter.writeLine(line.toArray(arrayLine));
        }
        csvWriter.close();
        log("writing results to file is Done! we've encountered errors calculating features for "+ skippedFileCount + " files.");
    }
    private List<String> getHeader() {
        List<String> header = FileData.getHeader();
        List<String> featuresName = features.stream().map(Feature::getName).collect(Collectors.toList());
        header.addAll(featuresName);
        return header;
    }
    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public static void run(List<Feature> features, String filesDataPath, String apiEntropiesCSV, String outputPath, String discussionMetricsCSV, String apiPostPositionsDigestCSV) throws IOException, GitAPIException {
        APIChallengeCalculator acc = new APIChallengeCalculator(apiEntropiesCSV, discussionMetricsCSV, apiPostPositionsDigestCSV);
        PostReleaseDefectPrediction prdp = new PostReleaseDefectPrediction(acc);
        prdp.setFeatures(features);
        prdp.preprocess();
        prdp.loadFilesData(filesDataPath);
        prdp.computeFileChallenges();
        prdp.writeResultsToFile(outputPath);

    }
}
