/**
 * Created by HTahmooresi on 10/2/2018.
 */

package main;

import main.sourcecode.CommitAPIExtractor;
import main.utils.Discussion;
import main.utils.Feature;
import main.utils.Loggable;
import org.eclipse.jgit.api.errors.GitAPIException;
import utility.StaticSharedData;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Runner extends Loggable {
    public static final String SEP = FileSystems.getDefault().getSeparator();
    private final String projectPath;
    private final String commitGuruPath;

    public Runner(String projectPath, String commitGuruPath) {
        this.projectPath = projectPath;
        this.commitGuruPath = commitGuruPath;
    }

    private List<Feature> getFeatures() {
        List<Feature> features = new ArrayList<>();
        features.add(new Feature("scoreFeature", Comparator.comparingInt(Discussion::getScore), 1));
        features.add(new Feature("upVotesFeature", Comparator.comparingInt(Discussion::getUpVotes), 2));
        features.add(new Feature("downVotesFeature", Comparator.comparingInt(Discussion::getDownVotes), 3));
        features.add(new Feature("viewCountFeature", Comparator.comparingInt(Discussion::getViewCount), 4));
        features.add(new Feature("answerCountFeature", Comparator.comparingInt(Discussion::getAnswerCount), 5));
        features.add(new Feature("favoriteFeature", Comparator.comparingInt(Discussion::getFavoriteCount), 6));
        features.add(new Feature("commentCount", Comparator.comparingInt(Discussion::getCommentCount), 7));
        features.add(new Feature("acceptedAnswerScoreFeature", Comparator.comparingInt(Discussion::getAcceptedAnswerScore), 9));
        features.add(new Feature("maxScoreFeature", Comparator.comparingInt(Discussion::getMaxScoreAnswer), 10));
        features.add(new Feature("questionerReputationFeature", Comparator.comparingInt(Discussion::getQuestionerReputation), 11));
        Collections.sort(features);
        return features;
    }

    public void runJITDefectPrediction() throws IOException, GitAPIException {
//        Challenge.run(apiPostPositionsCSV, stormedDatasetPath); // never run this code! It takes 3 days!
//        log("++++ Running EntropyCalculator...");
//        EntropyCalculator.run(apiPostPositionsCSV, apiEntropiesCSV);
//        log("++++ Running APIQuestionDigestMapGenerator...");
//        APIQuestionDigestMapGenerator.run(apiPostPositionsCSV, discussionMetricsCSV, apiPostPositionsDigestCSV);
//        log("++++ Running CommitAPIExtractor...");
        CommitAPIExtractor.run(projectPath,
                StaticSharedData.projectsCommitApiDirectory + StaticSharedData.projectName + ".csv"); // too time consuming!
//        log("++++ Running JITDefectPrediction");
//        List<Feature> features = getFeatures();
//        JITDefectPrediction.run(features,
//                commitGuruPath,
//                StaticSharedData.projectsCommitApiDirectory+StaticSharedData.projectName+".csv",
//                StaticSharedData.apiEntropiesCSV,
//                StaticSharedData.resultsCSVDirectory+StaticSharedData.projectName+".csv",
//                StaticSharedData.discussionMetricsCSV,
//                StaticSharedData.apiPostPositionsDigestCSV);
    }

    public void runPostReleaseDefectPrediction() throws IOException, GitAPIException {
        FilePostReleaseDefectsExtractor.run(
                projectPath,
                commitGuruPath,
                15552000 /*Six months*/,
                StaticSharedData.releaseCommitHash,
                StaticSharedData.csvBaseDirectory+SEP+"post-release-files-data"+SEP+StaticSharedData.projectName+".csv");
        log("Now executing PostReleaseDefectPrediction...");
        PostReleaseDefectPrediction.run(getFeatures(),
                StaticSharedData.csvBaseDirectory + SEP + "post-release-files-data" + SEP + StaticSharedData.projectName + ".csv",
                StaticSharedData.apiEntropiesCSV,
                StaticSharedData.csvBaseDirectory + SEP + "post-release-results" + SEP + StaticSharedData.projectName + ".csv",
                StaticSharedData.discussionMetricsCSV,
                StaticSharedData.apiPostPositionsDigestCSV
        );
    }

    public static void main(String[] args) throws IOException, GitAPIException {
        new Runner(StaticSharedData.projectsGitDirectory + StaticSharedData.projectName + Runner.SEP + ".git",
                StaticSharedData.commitGuruDirectory + StaticSharedData.projectName + ".csv").runPostReleaseDefectPrediction();
    }
}
