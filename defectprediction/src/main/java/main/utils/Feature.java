package main.utils;

import java.util.Comparator;

public class Feature implements Comparable<Feature> {
    private Comparator<Discussion> comparator;
    private int columnPositionInCSV;
    private String name;

    public Feature(String name, Comparator<Discussion> comparator, int columnPositionInCSV) {
        this.comparator = comparator;
        this.columnPositionInCSV = columnPositionInCSV;
        this.name = name;
    }

    public Comparator<Discussion> getComparator() {
        return comparator;
    }

    public void setComparator(Comparator<Discussion> comparator) {
        this.comparator = comparator;
    }

    public int getColumnPositionInCSV() {
        return columnPositionInCSV;
    }

    public void setColumnPositionInCSV(int columnPositionInCSV) {
        this.columnPositionInCSV = columnPositionInCSV;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Feature o) {
        return this.columnPositionInCSV - o.columnPositionInCSV;
    }
}
