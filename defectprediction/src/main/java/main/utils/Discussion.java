package main.utils;

import org.apache.commons.csv.CSVRecord;


public class Discussion implements Comparable<Discussion> {
    private int id;
    private int score;
    private int upVotes;
    private int downVotes;
    private int viewCount;
    private int answerCount;
    private int commentCount;
    private int favoriteCount;
    private boolean isClosed;
    private int acceptedAnswerScore;
    private int maxScoreAnswer;
    private int questionerReputation;

    public void fillDiscussion(CSVRecord csvRecord) {
        id = Double.valueOf(csvRecord.get(1).isEmpty() ? "0" : csvRecord.get(1)).intValue();
        score = Double.valueOf(csvRecord.get(3).isEmpty() ? "0" : csvRecord.get(3)).intValue();
        viewCount = Double.valueOf(csvRecord.get(4).isEmpty() ? "0" : csvRecord.get(4)).intValue();
        answerCount = Double.valueOf(csvRecord.get(5).isEmpty() ? "0" : csvRecord.get(5)).intValue();
        commentCount = Double.valueOf(csvRecord.get(6).isEmpty() ? "0" : csvRecord.get(6)).intValue();
        favoriteCount = Double.valueOf(csvRecord.get(7).isEmpty() ? "0" : csvRecord.get(7)).intValue();
        isClosed = !csvRecord.get(8).isEmpty();
        acceptedAnswerScore = Double.valueOf(csvRecord.get(10).isEmpty() ? "0" : csvRecord.get(10)).intValue();
        maxScoreAnswer = Double.valueOf(csvRecord.get(11).isEmpty() ? "0" : csvRecord.get(11)).intValue();
        upVotes = Double.valueOf(csvRecord.get(12).isEmpty() ? "0" : csvRecord.get(12)).intValue();
        downVotes = Double.valueOf(csvRecord.get(13).isEmpty() ? "0" : csvRecord.get(13)).intValue();
        questionerReputation = Double.valueOf(csvRecord.get(14).isEmpty() ? "0" : csvRecord.get(14)).intValue();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Discussion o) {
        return (this.score - o.score);
    }

    public int getQuestionerReputation() {
        return questionerReputation;
    }

    public void setQuestionerReputation(int questionerReputation) {
        this.questionerReputation = questionerReputation;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }

    public int getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(int downVotes) {
        this.downVotes = downVotes;
    }

    public int getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(int upVotes) {
        this.upVotes = upVotes;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public int getAcceptedAnswerScore() {
        return acceptedAnswerScore;
    }

    public void setAcceptedAnswerScore(int acceptedAnswerScore) {
        this.acceptedAnswerScore = acceptedAnswerScore;
    }

    public int getMaxScoreAnswer() {
        return maxScoreAnswer;
    }

    public void setMaxScoreAnswer(int maxScoreAnswer) {
        this.maxScoreAnswer = maxScoreAnswer;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getClosed() {
        if (this.isClosed()) {
            return 1;
        }
        return 0;
    }
}
