package main.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;


public class CSVWriter {
    private FileOutputStream fileStream;
    private PrintStream printStream;
    private String delimeter;

    public CSVWriter(String delimeter) {
        this.delimeter = delimeter;
    }

    public CSVWriter() {
        this.delimeter = ",";
    }

    public void initializeCSVPrinter(String path, List<String> header) throws FileNotFoundException {
        fileStream = new FileOutputStream(path);
        printStream = new PrintStream(fileStream);
        printStream.println(String.join(this.delimeter, header));
    }

    public void writeLine(String[] line) {
        printStream.println(String.join(this.delimeter, line));
    }

    public void close() throws IOException {
        fileStream.close();
        printStream.close();
    }
}
