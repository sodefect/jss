package main.utils;

import java.sql.Timestamp;


public class Loggable {
    public void log(String message) {
        System.out.println(new Timestamp(new java.util.Date().getTime()) + " - " + message);
    }
}
