package main;

import main.utils.Loggable;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EntropyCalculator extends Loggable {

    private Map<String, Double> apiEntropy = new HashMap<>();
    private Map<String, Integer> overallApiFrequency = new HashMap<>();
    private String path;

    public EntropyCalculator(String path) throws FileNotFoundException {
        this.path = path;
    }

    public void fetchAPIQuestionsFromCSVAndCalculateOverallApiFrequency() throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            String api = csvRecord.get(1);
            int count = Integer.valueOf(csvRecord.get(2));
            overallApiFrequency.put(api, overallApiFrequency.getOrDefault(api, 0) + count);
        }
        log("all data has been fetched and overall frequency was calculated");
        csvParser.close();
    }

    public void fetchAPIQuestionsFromCSVAndCalculateEntropy() throws IOException {
        log("fetchAPIQuestionsFromCSVAndCalculateEntropy");
        CSVParser csvParser = CSVReader.getCSVParser(path);
        Map<String, Integer> countInOneDiscussion = new HashMap<>();
        int lastQuestionId = -1;
        for (CSVRecord csvRecord : csvParser) {
            int currentQuestionId = Integer.valueOf(csvRecord.get(0));
            String api = csvRecord.get(1);
            int count = Integer.valueOf(csvRecord.get(2));

            if (currentQuestionId != lastQuestionId && lastQuestionId != -1) {
                for (String apiInQuestion : countInOneDiscussion.keySet()) {
                    double prob = (double) countInOneDiscussion.get(apiInQuestion) / (double) overallApiFrequency.get(apiInQuestion);
                    if (prob != 0.0) {
                        apiEntropy.put(apiInQuestion, apiEntropy.getOrDefault(apiInQuestion, 0.0) + (-1) * prob * (Math.log(prob)));
                    }
                }
                countInOneDiscussion = new HashMap<>();
            }
            countInOneDiscussion.put(api, countInOneDiscussion.getOrDefault(api, 0) + count);
            lastQuestionId = currentQuestionId;

        }
        csvParser.close();
    }

    public void writeEntropiesToFile(String path) throws IOException {
        FileOutputStream fileStream = new FileOutputStream(path);
        PrintStream printStream = new PrintStream(fileStream);
        printStream.println("API,Entropy");
        for (String api : apiEntropy.keySet()) {
            printStream.println(api + "," + apiEntropy.get(api));
        }
        printStream.close();
        fileStream.close();
    }

    public static void run(String apiPostPositionCSVPath, String outputPath) throws IOException {
        EntropyCalculator ec = new EntropyCalculator(apiPostPositionCSVPath);
        ec.fetchAPIQuestionsFromCSVAndCalculateOverallApiFrequency();
        ec.fetchAPIQuestionsFromCSVAndCalculateEntropy();
        ec.writeEntropiesToFile(outputPath);

    }

    public static void main(String[] args) throws IOException {
        EntropyCalculator.run("api-post-positions.csv", "api-entropies.csv");
    }
}
