package main.sourcecode;

import buggycodeannotator.CommitFinder;
import main.utils.CSVWriter;
import main.utils.Loggable;
import org.eclipse.jgit.api.errors.GitAPIException;
import java.io.IOException;
import java.util.*;

public class CommitAPIExtractor extends Loggable {


    private final String outputPath;
    private String gitProjectPath;
    private Map<String, List<String>> commitAPIs;

    public CommitAPIExtractor(String gitProjectPath, String outputPath) {
        this.gitProjectPath = gitProjectPath;
        this.outputPath = outputPath;
    }

    public void extractApis() throws IOException, GitAPIException {

        CommitFinder commitFinder = new CommitFinder(gitProjectPath);
        commitFinder.initializeRepository();

        /* Run Program */
        log("Loading all commits and their APIs");
        commitFinder.extractCommitAPIs();
        log("loading commits and their APIs done");
        // commit --> List of APIs
        Map<String, List<String>> addedOrModifiedAPIs = commitFinder.getCommitAPIs();
        Map<String, List<String>> deletedAPIs = commitFinder.getDeletedCommitAPIs();
        commitAPIs = new HashMap<>();
        Set<String> commitKeys = new TreeSet<>();
        commitKeys.addAll(addedOrModifiedAPIs.keySet());
        commitKeys.addAll(deletedAPIs.keySet());
        for (String commitHash : commitKeys){
            List<String> apis = new ArrayList<>();
            if (addedOrModifiedAPIs.containsKey(commitHash)){
                apis.addAll(addedOrModifiedAPIs.get(commitHash));
            }
            if (deletedAPIs.containsKey(commitHash)){
                apis.addAll(deletedAPIs.get(commitHash));
            }
            commitAPIs.put(commitHash, apis);
        }
    }

    public void saveToFile() throws IOException {
        log("Writing CommitAPIExtractor results to file");
        CSVWriter csvWriter = new CSVWriter();
        List<String> header = new ArrayList<>();
        header.add("commitHash");
        header.add("APIs");
        csvWriter.initializeCSVPrinter(outputPath, header);
        for (String commit : commitAPIs.keySet()) {
            csvWriter.writeLine(new String[]{commit, String.join(" ", commitAPIs.get(commit))});
        }
        csvWriter.close();
        log("writing CommitAPIExtractor results to file is Done!");
    }

    //    public static void main (String[] args) throws IOException, GitAPIException {
//        CommitAPIExtractor cae = new CommitAPIExtractor("C:\\Research\\Dataset\\commit.guru\\repos\\AmazeFileManager\\.git", "C:\\Research\\Dataset\\commit.guru\\extracted\\AmazeFileManager-commit-apis.csv");
//        cae.extractApis();
//        cae.saveToFile();
//    }
    public static void run(String projectGitPath, String outputPath) throws IOException, GitAPIException {
        CommitAPIExtractor cae = new CommitAPIExtractor(projectGitPath, outputPath);
        cae.extractApis();
        cae.saveToFile();
    }
}
