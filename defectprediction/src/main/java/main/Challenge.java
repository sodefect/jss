package main;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.jayway.jsonpath.JsonPath;
import utility.JsonPathIndexHelper;
import utility.StaticSharedData;

public class Challenge {
    private static HashMap<String, Double> scores = new HashMap<>();
    private static HashMap<String, Integer> numInCode = new HashMap<>();
    private static HashMap<String, Integer> numInCodeInAnswers = new HashMap<>();

    private static HashMap<String, String> apiType = new HashMap<>();
    private static HashMap<String, Integer> numInText = new HashMap<>();
    private static HashMap<String, Integer> numInTextInAnswers = new HashMap<>();
    private static HashMap<String, Integer> numInTitle = new HashMap<>();
    private static HashMap<String, Double> entropies = new HashMap<>();
    private static HashMap<String, Double> postScores = new HashMap<>();
    private static HashMap<String, Integer> numInAllCodes = new HashMap<>();
    private static HashMap<String, Integer> numInAllText = new HashMap<>();
    private static HashMap<String, Integer> numInAllTitle = new HashMap<>();
    private static HashMap<String, Integer> numberOfPostsContainingKey = new HashMap<>();

    public static final int IN_TITLE = 1;
    public static final int IN_TEXT = 2;
    public static final int IN_CODE = 3;
    public static final int IN_ANSWERS_TEXT = 4;
    public static final int IN_ANSWERS_CODE = 5;

    public static HashMap<String, Integer> apiTypeToIntMap = new HashMap<String, Integer>();
    private static int numberOfAllDiscussions = 0;

    private static int ALPHA_COEFFICIENT = 10;
    private static int BETA_COEFFICIENT = 4;
    private static int GAMMA_COEFFICIENT = 1;

    {
        Challenge.apiTypeToIntMap.put("unknown", 0);
        Challenge.apiTypeToIntMap.put("ClassDeclaration", 1);
        Challenge.apiTypeToIntMap.put("MethodInvocation", 2);
        Challenge.apiTypeToIntMap.put("Annotation", 3);
        Challenge.apiTypeToIntMap.put("Import", 4);
        Challenge.apiTypeToIntMap.put("ClassUsage", 5);
        Challenge.apiTypeToIntMap.put("MethodDeclarations", 6);
    }

    public static void run(String outputCSV, String StormedDatasetPath) throws IOException {


        File jsons = new File(StormedDatasetPath);
        File[] jsonFiles = jsons.listFiles();
        assert jsonFiles != null;
        FileOutputStream fileStream = new FileOutputStream(outputCSV);
        PrintStream printStream = new PrintStream(fileStream);
        printStream.println("PostId,API,Count,Position, ApiType");
        int count = 0;
        for (File jsonFile : jsonFiles) {
            if (jsonFile.isFile()) {
                try {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(new JSONTokener(new InputStreamReader(new FileInputStream(jsonFile))));
                    } catch (Exception e) {
                        continue;
                    }
                    int id = JsonPath.read(json.toString(), "$.question.id");
                    JsonPathIndexHelper.indexTitle(json, numInCode, numInTitle);
                    indexCodeElements(json, numInCode, numInText);
                    indexCodeElementsForAnswers(json, numInCodeInAnswers, numInTextInAnswers);
                    saveToFile(printStream, Integer.toString(id));
                    count++;
                    if (count % 1000 == 0) {
                        System.out.println(new Timestamp(new Date().getTime()) + " - " + String.valueOf(count) + " items processed");
                    }
                    emptyAllHashes();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            }
        }
        printStream.close();
        fileStream.close();
    }

    public static void main(String[] args) throws IOException {
        Challenge.run("api-post-positions.csv", StaticSharedData.STORMED_DATASET_PATH);
    }

    private static void indexCodeElements(JSONObject json, HashMap<String, Integer> numInCode, HashMap<String, Integer> numInText) {
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ImportDeclarationNode')].identifier..name", "Import", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='AnnotationNode')].identifier..name", "Annotation", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ClassDeclarationNode')].identifier.name", "ClassDeclaration", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='MethodInvocationNode')].identifier.name", "MethodInvocation", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ParameterizedTypeNode')].identifier.name", "ClassUsage", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='CatchTypeNode')]..name", "ClassUsage", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.question.informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='MethodDeclarationNode')].identifier.name", "MethodDeclarations", json, numInCode, apiType);

        JsonPathIndexHelper.indexText("$.question.informationUnits.", json, numInCode, numInText);
    }

    private static void indexCodeElementsForAnswers(JSONObject json, HashMap<String, Integer> numInCode, HashMap<String, Integer> numInText) {
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ImportDeclarationNode')].identifier..name", "Import", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='AnnotationNode')].identifier..name", "Annotation", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ClassDeclarationNode')].identifier.name", "ClassDeclaration", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='MethodInvocationNode')].identifier.name", "MethodInvocation", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='ParameterizedTypeNode')].identifier.name", "ClassUsage", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='CatchTypeNode')]..name", "ClassUsage", json, numInCode, apiType);
        JsonPathIndexHelper.indexCodeElement("$.answers[?(@.informationUnits)].informationUnits[?(@.type=='CodeTaggedUnit')].astNode.." + "[?(@.type=='MethodDeclarationNode')].identifier.name", "MethodDeclarations", json, numInCode, apiType);

        JsonPathIndexHelper.indexText("$.answers[?(@.informationUnits)].informationUnits.", json, numInCode, numInText);
    }


    private static void saveToFile(PrintStream printStream, String postId) {
        for (String key : numInTitle.keySet()) {
            printStream.println(postId + "," + key + "," + numInTitle.getOrDefault(key, 0) + "," + IN_TITLE + "," + apiTypeToIntMap.get(apiType.getOrDefault(key, "unknown")));
        }
        for (String key : numInText.keySet()) {
            printStream.println(postId + "," + key + "," + numInText.getOrDefault(key, 0) + "," + IN_TEXT + "," + apiTypeToIntMap.get(apiType.getOrDefault(key, "unknown")));
        }
        for (String key : numInCode.keySet()) {
            printStream.println(postId + "," + key + "," + numInCode.getOrDefault(key, 0) + "," + IN_CODE + "," + apiTypeToIntMap.get(apiType.getOrDefault(key, "unknown")));
        }
        for (String key : numInTextInAnswers.keySet()) {
            printStream.println(postId + "," + key + "," + numInTextInAnswers.getOrDefault(key, 0) + "," + IN_ANSWERS_TEXT + "," + apiTypeToIntMap.get(apiType.getOrDefault(key, "unknown")));
        }
        for (String key : numInCodeInAnswers.keySet()) {
            printStream.println(postId + "," + key + "," + numInCodeInAnswers.getOrDefault(key, 0) + "," + IN_ANSWERS_CODE + "," + apiTypeToIntMap.get(apiType.getOrDefault(key, "unknown")));
        }

    }

    private static void emptyAllHashes() {
        JsonPathIndexHelper.addToNum(numberOfPostsContainingKey, numInCode.keySet());
        numInCode = new HashMap<>();
        numInCodeInAnswers = new HashMap<>();
        numInTextInAnswers = new HashMap<>();
        numInText = new HashMap<>();
        numInTitle = new HashMap<>();
        apiType = new HashMap<>();
    }

    private static void updateScore(String id) {
        for (String key : numInCode.keySet()) {
            try {
                double alpha = ALPHA_COEFFICIENT * getNum(numInTitle, key);
                double beta = BETA_COEFFICIENT * getNum(numInText, key);
                double gamma = GAMMA_COEFFICIENT * getNum(numInCode, key);
                double entropy = 0.0;
                double postScore = 1.0;

                entropy = entropies.getOrDefault(key, 0.0);

                if (postScores.containsKey(id)) {
                    postScore = postScores.get(id);
                }

                double newScore = (((alpha + beta + gamma) / 15) * postScore);
                if (scores.containsKey(key)) {
                    scores.put(key, scores.get(key) + newScore);
                } else {
                    scores.put(key, newScore);
                }
            } catch (Exception e) {
//				e.printStackTrace(System.out);
            }
        }
    }

    private static void printScores() throws FileNotFoundException {
        FileOutputStream f = new FileOutputStream("final.csv");
        PrintStream p = new PrintStream(f);
        for (String key : scores.keySet()) {
//			System.out.println(key);
//			System.out.println(numberOfPostsContainingKey.get(key));
//			System.out.println(numberOfAllDiscussions);
            double l = numberOfAllDiscussions / numberOfPostsContainingKey.get(key);
            double sc = scores.get(key) * l;
            try {
                if (numberOfPostsContainingKey.get(key) < StaticSharedData.POST_GENERAL_TOPIC_THRESHOLD)
                    continue;
                p.println(key + "," + String.valueOf(scores.get(key)) + "," + String.valueOf(l) + "," + String.valueOf(sc) + "," + entropies.get(key) + ","
                        + numInAllTitle.get(key) + "," + numInAllText.get(key) + "," + numInAllCodes.get(key) + ","
                        + numberOfPostsContainingKey.get(key));
            } catch (Exception e) {
//				e.printStackTrace(System.out);
            }
        }
        p.flush();
        p.close();
    }

    private static int getNum(HashMap<String, Integer> hash, String key) {
        return hash.getOrDefault(key, 0);
    }

}
