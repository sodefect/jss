package main;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;

public class Search {
    static HashMap<String, Double> entropies = new HashMap<String, Double>();

    public static HashMap<String, Float> search(Query query) throws IOException {
        String indexName = "index";
        Directory directory = new SimpleFSDirectory(Paths.get(indexName));
        return searchQuery(query, directory);
    }

    private static HashMap<String, Float> searchQuery(Query q, Directory directory) throws IOException {
        int hitsPerPage = 10;
        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(indexReader);
        HashMap<String, Float> answer = new HashMap<>();
        try {
            TopDocs docs = searcher.search(q, hitsPerPage);
            ScoreDoc[] hits = docs.scoreDocs;
//			System.out.println("Found " + hits.length + " hits.");
            for (int i = 0; i < hits.length; ++i) {
                int docId = hits[i].doc;
                Document d = searcher.doc(docId);
                if (!answer.containsKey(d.get("unit_id"))) {
                    answer.put(d.get("unit_id"), hits[i].score);
                }
            }
        } catch (Exception e) {
            indexReader.close();
            directory.close();
//			e.printStackTrace(System.out);
            return answer;
        }
        indexReader.close();
        directory.close();
        return answer;
    }
}
