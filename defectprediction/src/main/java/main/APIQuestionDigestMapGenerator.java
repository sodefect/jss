package main;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;


public class APIQuestionDigestMapGenerator {
    private static Set<String> apis = new TreeSet<>();
    //    private static Integer soQuestionIds = new TreeSet<>();
    private static Map<String, Integer> apiQuestionPositionMap = new LinkedHashMap<>();
    private static Map<String, Integer> selectedApiQuestionMap = new LinkedHashMap<>();
//    private static Map<Integer, Integer> acceptableQuestions = new HashMap<>();
    private static PrintStream printStream;
    private static FileOutputStream fileStream;

    private static CSVParser getCSVParser(String path) throws IOException {
        FileReader filereader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(filereader);
        bufferedReader.readLine();
        return new CSVParser(bufferedReader, CSVFormat.DEFAULT);
    }

    private static void initializeCSVPrinter(String outputPath) throws FileNotFoundException {
        fileStream = new FileOutputStream(outputPath);
        printStream = new PrintStream(fileStream);
        printStream.println("PostId,API,RelationType");
    }

    public static void run(String ApiQuestionsCSVPath, String discussionMetricsCSVPath, String outputPath) throws IOException {
        log("extractQuestionScores...");
        log("Questions Scores fetched. Fetching api question position digest map...");
        try {
            CSVParser csvParser = getCSVParser(ApiQuestionsCSVPath);
            for (CSVRecord csvRecord : csvParser) {
                Integer soQuestionId = Integer.valueOf(csvRecord.get(0));
                String api = csvRecord.get(1);
                Integer count = Integer.valueOf(csvRecord.get(2));
                Integer position = Integer.valueOf(csvRecord.get(3));
                apiQuestionPositionMap.put(soQuestionId + "," + api + "," + position, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int counter = 0;
        initializeCSVPrinter(outputPath);
        int overallCount = apiQuestionPositionMap.keySet().size();
        log("All data fetched. Now generating digest csv of api question map over " + overallCount + " items...");
        Integer lastQuestionId = null;
        int tempCounter = 0;
        for (String data : apiQuestionPositionMap.keySet()) {
            counter++;
            tempCounter++;
            Integer soQuestionId = Integer.valueOf(data.split(",")[0]);
            if (!soQuestionId.equals(lastQuestionId)) {
                tempCounter = 0;
                log(counter + " / " + overallCount + " items processed. Now writing them to file");
                writeToCSV();
                selectedApiQuestionMap = new TreeMap<>();
            }

            String api = data.split(",")[1];
            Integer position = Integer.valueOf(data.split(",")[2]);
            Integer count = apiQuestionPositionMap.get(data);


            Integer shouldKeep = shouldKeepApiQuestionAssociation(soQuestionId, api, position, count);
            if (shouldKeep > 0) {
                int currentStatus = selectedApiQuestionMap.getOrDefault(getMapKey(soQuestionId, api), 1000);
                selectedApiQuestionMap.put(getMapKey(soQuestionId, api), Math.min(shouldKeep, currentStatus));
            }
            lastQuestionId = soQuestionId;
        }
        writeToCSV();
        log("digest csv of api question map generated.");
        printStream.close();
        fileStream.close();

    }

    public static String getMapKey(Integer soQuestionId, String api) {
        return soQuestionId + "," + api;
    }

    public static void main(String[] args) throws IOException {
        APIQuestionDigestMapGenerator.run("api-post-positions.csv", "so_query_aggregated.csv", "api-post-association.csv");
    }

    private static void writeToCSV() {
        for (String apiQuestion : selectedApiQuestionMap.keySet()) {
            printStream.println(apiQuestion + "," + selectedApiQuestionMap.get(apiQuestion));
        }
    }
//
//    private static boolean hasEnoughQuality(Integer soQuestionId) {
//        return acceptableQuestions.containsKey(soQuestionId);
//    }

    private static Integer shouldKeepApiQuestionAssociation(int soQuestionId, String api, int position, int count) {
        if (position == Challenge.IN_TITLE) {
            return 1;
        }
        if (position == Challenge.IN_CODE && occurredBefore(soQuestionId, api, Challenge.IN_TEXT)) {
            return 2;
        }
        if (position == Challenge.IN_ANSWERS_CODE && occurredBefore(soQuestionId, api, Challenge.IN_CODE)) {
            return 3;
        }
        if (position == Challenge.IN_ANSWERS_TEXT && occurredBefore(soQuestionId, api, Challenge.IN_CODE)) {
            return 4;
        }
        if (position == Challenge.IN_ANSWERS_TEXT && occurredBefore(soQuestionId, api, Challenge.IN_TEXT)) {
            return 5;
        }

        return -1;
    }

    private static boolean occurredBefore(int soQuestionId, String api, int position) {
        return apiQuestionPositionMap.containsKey(soQuestionId + "," + api + "," + position);
    }

    private static void log(String message) {
        System.out.println(new Timestamp(new Date().getTime()) + " - " + message);
    }
}
