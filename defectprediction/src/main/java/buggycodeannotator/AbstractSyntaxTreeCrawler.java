package buggycodeannotator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.eclipse.jdt.core.dom.*;
import org.json.simple.JSONObject;

public class AbstractSyntaxTreeCrawler {

    final static String CLASS_DECLARATION = "CLASS";
    final static String METHOD_INVOCATION = "METHOD";
    final static String CLASS_USAGE = "CLASS";
    final static String METHOD_DECLARATION = "METHOD";
    final static String PACKAGE_IMPORT = "IMPORTS";
    final static String ANNOTATION = "ANNOTATION";
    final static String NUM_OF_ALL_TERMS = "NumOfAllTerms";
    final static String EXTENSION = "CLASS";
    private JSONObject graph;
    private String code = "";
    private HashMap<String, Double> numInOneDiscussion = new HashMap<>();
    private HashMap<String, Double> entropies = new HashMap<>();
    private ArrayList<String> allAPI = new ArrayList<>();
    private ArrayList<String> allClasses = new ArrayList<>();
    private ArrayList<String> allMethodInvocations = new ArrayList<>();
    private ArrayList<String> allMethodDeclarations = new ArrayList<>();
    private ArrayList<String> allAnnotations = new ArrayList<>();
    private static AbstractSyntaxTreeCrawler abstractSyntaxTreeCrawler;
    Analyzer analyzer = this.analyzer = new Analyzer() {

        @Override
        protected TokenStreamComponents createComponents(String arg0) {
            Tokenizer source = new StandardTokenizer();
            return new TokenStreamComponents(source);
        }
    };
    private int counter;

    public static AbstractSyntaxTreeCrawler getInstance(){
        if (abstractSyntaxTreeCrawler == null){
            abstractSyntaxTreeCrawler = new AbstractSyntaxTreeCrawler();
        }
        return abstractSyntaxTreeCrawler;
    }

    public void reset(){
        allAPI = new ArrayList<>();
        allClasses = new ArrayList<>();
        allMethodInvocations = new ArrayList<>();
        allMethodDeclarations = new ArrayList<>();
        allAnnotations = new ArrayList<>();
        numInOneDiscussion = new HashMap<>();
        entropies = new HashMap<>();
    }

    public void buildAST(String code, ArrayList<Integer> lineNumbers) {
        this.reset();
        this.code = code;
        ASTParser parser = ASTParser.newParser(AST.JLS10);
        parser.setSource(code.toCharArray());
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

        cu.accept(new ASTVisitor() {
            // TODO: Check if this method works correctly
            public boolean visit(MethodInvocation node) {
                Name name = node.getName();
                int lineNumber = cu.getLineNumber(name.getStartPosition());
                if (lineNumber >= 175 && lineNumber < 177){
                    int a = 1;
                }
//				System.out.println("MethodInvocation: " + name + " line_number: " + cu.getLineNumber(name.getStartPosition()));
                if (lineNumbers.contains(lineNumber)) {
//                    System.out.println("MethodInvocation2: " + name);
                    addOneToDiscussionTerms(METHOD_INVOCATION + ":" + name);
                    allAPI.add(name.toString());
                }
                allMethodInvocations.add(name.toString());
                return true;
            }
            public boolean visit(SimpleType node) {
                Name name = node.getName();
                if (lineNumbers.contains(cu.getLineNumber(name.getStartPosition()))) {
//                    System.out.println("SimpleType2: " + name);
                    addOneToDiscussionTerms(CLASS_USAGE + ":" + name);
                    allAPI.add(name.toString());
                    allClasses.add(name.toString());
                }
                return true;
            }

//            public boolean visit(MethodDeclaration node) {
//                Name name = node.getName();
//                if (lineNumbers.contains(cu.getLineNumber(name.getStartPosition()))) {
////                    System.out.println("MethodDeclaration2: " + name);
//                    addOneToDiscussionTerms(METHOD_DECLARATION + ":" + name);
//                    allAPI.add(name.toString());
//                    allMethodDeclarations.add(name.toString());
//                }
//                return true;
//            }

//            public boolean visit(TypeDeclaration node) {
//                Name name = node.getName();
//                if (lineNumbers.contains(cu.getLineNumber(name.getStartPosition()))) {
////                    System.out.println("TypeDeclaration2: " + name);
//                    addOneToDiscussionTerms(CLASS_DECLARATION + ":" + name);
//                    allAPI.add(name.toString());
//                    allClasses.add(name.toString());
//                }
//                return true;
//            }

            public boolean visit(ImportDeclaration node) {
                Name name = node.getName();
                if (lineNumbers.contains(cu.getLineNumber(name.getStartPosition()))) {
//                    System.out.println("ImportDeclaration2: " + name);
                    addOneToDiscussionTerms(PACKAGE_IMPORT + ":" + name);
                    allAPI.add(name.toString());
                    allClasses.add(name.toString());
                }
                return true;
            }

            public boolean visit(MarkerAnnotation node) {
                Name name = node.getTypeName();
                if (lineNumbers.contains(cu.getLineNumber(name.getStartPosition()))) {
//					System.out.println("MarkerAnnotation2: " + name);
                    addOneToDiscussionTerms(ANNOTATION + ":" + name);
                    allAPI.add(name.toString());
                    allAnnotations.add(name.toString());
                }
                return true;
            }

        });


    }


    public Query getQuery() throws ParseException, IOException {
        return this.getQueryWithFields();
    }

    public void addOneToDiscussionTerms(String key) {
        if (numInOneDiscussion == null)
            numInOneDiscussion = new HashMap<>();
        else if (!numInOneDiscussion.containsKey(key)) {
            numInOneDiscussion.put(key, 1.0);
        } else {
            numInOneDiscussion.put(key, numInOneDiscussion.get(key) + 1.0);
        }
    }

    private static <K, V> Map<K, V> sortByValue(Map<K, V> map) {
        List<Entry<K, V>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Object>() {
            @SuppressWarnings("unchecked")
            public int compare(Object o1, Object o2) {
                return ((Comparable<V>) ((Entry<K, V>) (o1)).getValue()).compareTo(((Entry<K, V>) (o2)).getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Iterator<Entry<K, V>> it = list.iterator(); it.hasNext(); ) {
            Entry<K, V> entry = (Entry<K, V>) it.next();
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public ArrayList<String> getQueryNamesAndFields() throws IOException {

        for (String key : numInOneDiscussion.keySet()) {
            try {
                String[] total = key.split(":");
                numInOneDiscussion.put(key, (numInOneDiscussion.get(key) * (1 - entropies.get(total[1]))));
            } catch (NullPointerException e) {
                numInOneDiscussion.put(key, (numInOneDiscussion.get(key) * (1 - 0.0)));
            }
        }

        String querystr = "";
        ArrayList<String> res = new ArrayList<>();
        Map<String, Double> sorted = sortByValue(numInOneDiscussion);

        int num = this.counter;
        for (String key : sorted.keySet()) {
            if (num == counter) {
//				querystr = key;
                res.add(key);
                num--;
            } else if (num > 0) {
//				querystr = querystr + " AND " + key;
                res.add(key);
                num--;
            } else break;
        }

        return res;
    }

    public Query getQueryWithFields() throws ParseException, IOException {
        ArrayList<String> namesWithFields = this.getQueryNamesAndFields();
        if (namesWithFields.isEmpty()) {
            return null;
        }
        //TODO
        //String temp = "RAW:" + String.valueOf("'" + this.code + "'");
        //namesWithFields.add(temp);
        String result = "";
        for (String str : namesWithFields) {
            result += str + " ";
        }
        return (new QueryParser("", this.analyzer).parse(result));
    }

    public ArrayList<String> getAllAPIs() {
        return this.allAPI;
    }

    public ArrayList<String> getAllClasses() {
        return this.allClasses;
    }

    public ArrayList<String> getAllMethodDeclarations() {
        return this.allMethodDeclarations;
    }

    public ArrayList<String> getAllMethodInvocations() {
        return this.allMethodInvocations;
    }

    public ArrayList<String> getAllAnnotations() {
        return this.allAnnotations;
    }
}
