package buggycodeannotator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Random;

import org.apache.lucene.queryparser.classic.ParseException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import utility.StaticSharedData;
import utility.UtilFunctions;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesianLogisticRegression;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class InputHandler {

    public static void main(String[] args) throws IOException, GitAPIException, ParseException {

//        String path = StaticSharedData.PATH_TO_GIT_REPO;
//        /* Initialize Repository */
//        CommitFinder commitFinder = new CommitFinder(path);
//        commitFinder.initializeRepository();
//
//        /* Initialize variables from files **/
//        StaticSharedData.postScores = UtilFunctions.getPostScoresFromFile();
//        StaticSharedData.apiScores = UtilFunctions.getAPIScoresFromFile();
//
//        /* Run Program */
//        HashMap<RevCommit, String> allCommitsData = new HashMap<>();
//        commitFinder.annotateCommits(commitFinder.findAllCommits(), allCommitsData);
//
//        FileOutputStream fileStream = new FileOutputStream("result.csv");
//        PrintStream printStream = new PrintStream(fileStream);
//        printStream.println("commit_hash,class_score,method_declaration_score,"
//                + "method_invocation_score,annotation_score,post_score,edited_lines,buggy");
//        for (RevCommit commit : allCommitsData.keySet()) {
//            printStream.println(allCommitsData.get(commit));
//        }
//        printStream.close();
//        fileStream.close();
//        classifier();
    }

    private static void classifier() {
        try {
            DataSource dataset = new DataSource("our_result.csv");
            Instances data = dataset.getDataSet();
            data.deleteAttributeAt(0);
            data.setClassIndex(data.numAttributes() - 1);
            Classifier classifier = new BayesianLogisticRegression();
            classifier.buildClassifier(data);
            Evaluation eval = new Evaluation(data);
            eval.crossValidateModel(classifier, data, 10, new Random(1));
//    		System.out.println(eval.precision(1));
        } catch (Exception e) {
//    		e.printStackTrace(System.out);
        }

    }
}
