package com.apis;

public interface StackOverflowFire {
    void fire(String title, ApiChallenge apiChallenge);
}
