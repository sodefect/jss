package com.apis;

import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.*;
import com.intellij.ui.content.Content;
import com.apis.filter.FilterableConsoleViewToolWindow;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class ReadApisAction extends AnAction {


    @Override
    public void actionPerformed(AnActionEvent e) {
//        BrowserUtil.browse("https://stackoverflow.com/questions/ask");

        if (e.getProject() == null)
            return;

        InputStream inputStream;
        try {
            inputStream = new FileInputStream("C:/all_apis.csv");
            if (inputStream.available() < 1) {
                inputStream = getClass().getResourceAsStream("/all_apis.csv");
            }
        } catch (IOException ex) {
            inputStream = getClass().getResourceAsStream("/all_apis.csv");
        }

        Map<String, ApiChallenge> apiChallenges = ApiChallenge.getAllApiChallenges(inputStream);


//        final Editor editor = e.getRequiredData(CommonDataKeys.EDITOR);
//        CaretModel caretModel = editor.getCaretModel();

        // For searches from the editor, we should also get file type information
        // to help add scope to the search using the Stack overflow search syntax.
        //
        // https://stackoverflow.com/help/searching


//        FilterableConsoleViewToolWindow.newTab(e.getProject(), "Default");

        ToolWindow toolWindow = ToolWindowManager.getInstance(e.getProject()).getToolWindow("Api Console");
        ConsoleView consoleView = TextConsoleBuilderFactory.getInstance().createBuilder(e.getProject()).getConsole();
//        Content content = toolWindow.getContentManager().findContent("Apis Challenging Output");
        toolWindow.getContentManager().removeAllContents(true);

//        Content content = toolWindow.getContentManager().getFactory().createContent(consoleView.getComponent(), "Apis Challenging Output", true);
//        toolWindow.getContentManager().addContent(content);


//        PsiFile file = e.getData(CommonDataKeys.PSI_FILE);

        Project project = e.getProject();
        FileEditorManager manager = FileEditorManager.getInstance(project);
        VirtualFile[] files = manager.getSelectedFiles();
        if (files.length == 0) {
            consoleView.print("No file are open in the editor!\n", ConsoleViewContentType.NORMAL_OUTPUT);
            return;
        }
        PsiFile file = PsiManager.getInstance(project).findFile(files[0]);


        ConsoleView stackOverflowConsoleView = TextConsoleBuilderFactory.getInstance().createBuilder(e.getProject()).getConsole();
        Content stackOverflowLinksContent = toolWindow.getContentManager().getFactory().createContent(stackOverflowConsoleView.getComponent(), "StackOverflow Links", true);
        toolWindow.getContentManager().addContent(stackOverflowLinksContent);


        FilterableConsoleViewToolWindow filterableConsoleViewToolWindow = FilterableConsoleViewToolWindow.newTab(e.getProject(), "Apis Challenging Output");

        if (file != null && filterableConsoleViewToolWindow != null) {
            FilterableConsoleViewToolWindow.addApisContent(apiChallenges, filterableConsoleViewToolWindow.getFilterableConsoleView(), file, new StackOverflowFire() {
                @Override
                public void fire(String title, ApiChallenge apiChallenge) {
                    stackOverflowConsoleView.clear();
                    toolWindow.getContentManager().setSelectedContent(stackOverflowLinksContent);

                    stackOverflowConsoleView.print(title + "\n", ConsoleViewContentType.NORMAL_OUTPUT);

                    apiChallenge.tryStackOverflow(stackOverflowConsoleView);
                }
            });
        }

//      if(caretModel.getCurrentCaret().hasSelection())
//      {
//         String query = caretModel.getCurrentCaret().getSelectedText().replace(' ', '+') + languageTag;
//         BrowserUtil.browse("https://stackoverflow.com/search?q=" + query);
//      }


        toolWindow.show(new Runnable() {
            @Override
            public void run() {

            }
        });


//        int offset = editor.getCaretModel().getOffset();

//        final StringBuilder infoBuilder = new StringBuilder();
//        PsiElement element = file.findElementAt(offset);
//        infoBuilder.append("Element at caret: ").append(element).append("\n");
//        Messages.showMessageDialog(e.getProject(), infoBuilder.toString(), "PSI Info", null);


//        if (element != null) {
//            PsiMethod containingMethod = PsiTreeUtil.getParentOfType(element, PsiMethod.class);
//            infoBuilder
//                    .append("Containing method: ")
//                    .append(containingMethod != null ? containingMethod.getName() : "none")
//                    .append("\n");
//            if (containingMethod != null) {
//                PsiClass containingClass = containingMethod.getContainingClass();
//                infoBuilder
//                        .append("Containing class: ")
//                        .append(containingClass != null ? containingClass.getName() : "none")
//                        .append("\n");
//
//                infoBuilder.append("Local variables:\n");
//                containingMethod.accept(new JavaRecursiveElementVisitor() {
//                    @Override
//                    public void visitLocalVariable(PsiLocalVariable variable) {
//                        super.visitLocalVariable(variable);
//                        infoBuilder.append(variable.getName()).append("\n");
//                    }
//                });
//            }
//        }

    }

//    @Override
//    public void update(AnActionEvent e) {
//        final Editor editor = e.getRequiredData(CommonDataKeys.EDITOR);
//        CaretModel caretModel = editor.getCaretModel();
//        e.getPresentation().setEnabledAndVisible(caretModel.getCurrentCaret().hasSelection());
//    }
}
