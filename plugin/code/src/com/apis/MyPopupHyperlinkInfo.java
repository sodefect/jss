package com.apis;

import com.intellij.execution.filters.HyperlinkInfo;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.lang.Language;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.fileTypes.PlainTextFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.ui.BrowserHyperlinkListener;
import com.intellij.ui.EditorTextField;
import com.intellij.ui.content.Content;

import javax.swing.*;
import java.awt.*;

import static com.intellij.openapi.wm.WindowManager.*;

public class MyPopupHyperlinkInfo implements HyperlinkInfo {
    private final String myTitle;
    private final ApiChallenge apiChallenge;
    private String myText;
    private final ConsoleView stackOverflowConsoleView;
    private final ToolWindow toolWindow;
    private final Content stackOverflowLinksContent;

    public MyPopupHyperlinkInfo(String title, ApiChallenge apiChallenge, ConsoleView stackOverflowConsoleView, ToolWindow toolWindow, Content stackOverflowLinksContent) {
        myTitle = title;
        this.apiChallenge = apiChallenge;
        this.stackOverflowConsoleView = stackOverflowConsoleView;
        this.toolWindow = toolWindow;
        this.stackOverflowLinksContent = stackOverflowLinksContent;

        StringBuilder res = new StringBuilder();
//        for (String s : links) {
//            res.append(linkIfy(s)).append("\n");
//        }

        myText = res.toString();
    }

    @Override
    public void navigate(Project project) {
        ApplicationManager.getApplication().invokeLater(() -> {
//            new Html("");


//            showBalloon(project);

        });
    }

    private void showBalloon(Project project) {
        Document document = EditorFactory.getInstance().createDocument(StringUtil.formatLinks(myText));

        Language language = Language.findLanguageByID("HTML");
        FileType fileType = language != null ? language.getAssociatedFileType() : PlainTextFileType.INSTANCE;


        EditorTextField textField = new EditorTextField(document, project, fileType, true, false) {
            @Override
            protected EditorEx createEditor() {
                EditorEx editor = super.createEditor();
//                    EditorHyperlinkSupport.get(editor).highlightHyperlinks(null, 1, 10);
                editor.getScrollPane().setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
                editor.getScrollPane().setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                editor.getSettings().setUseSoftWraps(true);
                return editor;
            }
        };


        JFrame frame = getInstance().getFrame(project);
        if (frame != null) {
            Dimension size = frame.getSize();
            if (size != null) {
                textField.setPreferredSize(new Dimension(size.width / 2, size.height / 2));
            }
        }

//            JBPopupFactory.getInstance()
//                    .createComponentPopupBuilder(textField, textField)
//                    .setTitle(myTitle)
//                    .setResizable(true)
//                    .setMovable(true)
//                    .setRequestFocus(true)
//                    .createPopup()
//                    .showCenteredInCurrentWindow(project);

        JBPopupFactory.getInstance()
                .createHtmlTextBalloonBuilder(myText, MessageType.INFO, new BrowserHyperlinkListener())
//                    .createComponentPopupBuilder(textField, textField)
                .setTitle(myTitle)
//                    .setResizable(true)
//                    .setMovable(true)
                .setRequestFocus(true)
//                    .createPopup()
                .setCloseButtonEnabled(true)
                .createBalloon()
                .showInCenterOf(getInstance().getFrame(project).getRootPane());

//            Balloon balloon = JBPopupFactory.getInstance().createHtmlTextBalloonBuilder(ProfilerBundle.message("action.gw.profiler.AutoAnalysis.Complete", linkIfy(_file.getName())), AllIcons.Icons.AUTO_ANALYSIS, Color.CYAN, new HyperlinkListener() {
//                @Override
//                public void hyperlinkUpdate(HyperlinkEvent e) {
//                    if(! (e.getEventType() == ACTIVATED)){return;}
//                    FileEditorManager.getInstance(_profilerManager.getProject()).openFile(_file, true);
//                }
//            }).setCloseButtonEnabled(true)
//                    .setDisposable(_profilerManager.getProject())
//                    .setHideOnAction(true)
//                    .setHideOnClickOutside(true)
//                    .setHideOnLinkClick(true)
//                    .setHideOnKeyOutside(true)
//                    .createBalloon();
//
//            balloon.showInCenterOf(project);
//            balloon.show(new RelativePoint(selectedCompoenent, new Point(50, selectedCompoenent.getY() * 3 / 2)), Balloon.Position.above);

    }


}