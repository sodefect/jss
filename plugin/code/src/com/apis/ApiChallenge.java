package com.apis;

import com.intellij.execution.filters.BrowserHyperlinkInfo;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.util.io.HttpRequests;
import com.intellij.util.io.RequestBuilder;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ApiChallenge {
    public String name;
    public String challenging;
    public String[] stackOverflowIds;

    public ApiChallenge(String name, String challenging, String[] stackOverflowIds) {
        this.name = name;
        this.challenging = challenging;
        this.stackOverflowIds = stackOverflowIds;
    }

    public String getName() {
        return name;
    }

    public String getChallenging() {
        return challenging;
    }


//    private static Map<String, ApiChallenge> apiChallenges = null;

    public static Map<String, ApiChallenge> getAllApiChallenges(InputStream resource) {
//        if (apiChallenges != null)
//            return apiChallenges;

        Map<String, ApiChallenge> apiChallenges = new HashMap<>();

        try {
            String result = new BufferedReader(new InputStreamReader(resource))
                    .lines().collect(Collectors.joining("\n"));

            for (String row : result.split("\\n")) {
                String[] data = row.split(",");
                if (data.length > 1) {
//                    System.out.println(data[0] + ":" + data[1]);
                    ApiChallenge item = new ApiChallenge(data[0], data[1], Arrays.copyOfRange(data, 2, data.length));
                    apiChallenges.put(data[0], item);
                }
            }
            resource.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return apiChallenges;
    }

    public String[] getStackOverflowLinks() {
        String[] res = new String[stackOverflowIds.length];
        for (int i = 0; i < stackOverflowIds.length; i++) {
            res[i] = "https://stackoverflow.com/questions/" + stackOverflowIds[i];
        }
        return res;
    }

    public String getStackOverflowLinksHtml() {
        StringBuilder res = new StringBuilder();
        for (String s : this.stackOverflowIds) {
            res.append(
//                    Utils.linkIfy(
                    "https://stackoverflow.com/questions/" + s
//                    )
            ).append("\n");
        }
        return res.toString();
    }


    public void tryStackOverflow(ConsoleView stackOverflowConsoleView) {
        if (stackOverflowIds == null || stackOverflowIds.length == 0)
            return;

        String joined = String.join(";", stackOverflowIds);
        String url = "https://api.stackexchange.com/2.2/questions/" + joined + "?order=desc&sort=activity&site=stackoverflow";

        RequestBuilder test = HttpRequests.request(url);
        try {
            test.connect(request -> {
                InputStream is = request.getInputStream();

                String result = new BufferedReader(new InputStreamReader(is))
                        .lines().collect(Collectors.joining("\n"));

                try {
                    JSONObject json = new JSONObject(result);
                    JSONArray items = json.getJSONArray("items");
                    for (int i = 0; i < items.length(); i++) {
                        StackOverflowItem.getItem(items.getJSONObject(i)).printInConsole(stackOverflowConsoleView);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return null;
            });
        } catch (IOException e) {
            e.printStackTrace();
            for (String link : getStackOverflowLinks()) {
                stackOverflowConsoleView.printHyperlink(link, new BrowserHyperlinkInfo(link));
                stackOverflowConsoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
            }
        }

    }
}
