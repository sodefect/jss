package com.apis;

public class Utils {
    public static String linkIfy(String s) {
        final String A_HREF = "<a href=\"";
        final String HREF_CLOSED = "\">";
        final String HREF_END = "</a>";
        return A_HREF.concat(s).concat(HREF_CLOSED).concat(s).concat(HREF_END);
    }
}
