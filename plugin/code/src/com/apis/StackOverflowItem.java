package com.apis;

import com.intellij.execution.filters.BrowserHyperlinkInfo;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class StackOverflowItem {

    public final long questionId;
    public final String title;
    public final String link;

    private StackOverflowItem(long questionId, String title, String link) {

        this.questionId = questionId;
        this.title = title;
        this.link = link;
    }

    @Nullable
    public static StackOverflowItem getItem(@NotNull JSONObject jsonObject) {
        try {
            return new StackOverflowItem(jsonObject.getLong("question_id"), jsonObject.getString("title"), jsonObject.getString("link"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void printInConsole(ConsoleView stackOverflowConsoleView) {
        stackOverflowConsoleView.printHyperlink(title, new BrowserHyperlinkInfo(link));
        stackOverflowConsoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
    }
}
