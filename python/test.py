import pandas as pd
from scipy import stats
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import seaborn as sb
from sklearn import metrics
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import pandas as pd
from scipy import stats
from sklearn.utils import column_or_1d
import numpy as np

sb.set_style('whitegrid')
df_initial = pd.read_csv('C:\\Research\\Dataset\\CSVs\\discussion-metrics.csv',index_col='question_Id')
del df_initial['accepted_answer_Id']
del df_initial['question_ClosedDate']
del df_initial['question_CreationDate']
del df_initial['Unnamed: 0']
df = df_initial.fillna(0)
df = df.query('question_Score >= 1.0')
df = df.apply(lambda x: ((x - x.min()) / (x.max() - x.min()))) + df[df > 0].min()
new_d =  df[['question_upvotes','question_Score','question_AnswerCount','accepted_answer_Score','max_score_answer','question_ViewCount','question_FavoriteCount']]
u, s, vh = np.linalg.svd(new_d, full_matrices=True)
print(1)