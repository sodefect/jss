select
question.Id as question_Id, 
MAX(question.CreationDate) as question_CreationDate,
MAX(question.Score) as question_Score,
MAX(question.ViewCount) as question_ViewCount,
MAX(question.AnswerCount) as question_AnswerCount,
MAX(question.CommentCount) as question_CommentCount,
MAX(question.FavoriteCount) as question_FavoriteCount,
MAX(question.ClosedDate) as question_ClosedDate,
MAX(accepted_answer.Id) as accepted_answer_Id, 
MAX(accepted_answer.Score) as accepted_answer_Score,
MAX(all_answers.Score) as max_score_answer,
SUM(CASE WHEN question_votes.VoteTypeId = 2 THEN 1 ELSE 0 END) as question_upvotes,
SUM(CASE WHEN question_votes.VoteTypeId = 3 THEN 1 ELSE 0 END) as question_downvotes,
MAX(questioner.Reputation) as questioner_reputation

from Posts question
LEFT JOIN Posts accepted_answer ON question.AcceptedAnswerId = accepted_answer.Id
RIGHT JOIN Posts all_answers ON all_answers.ParentId = question.Id
RIGHT JOIN Votes question_votes ON question_votes.PostId = question.Id
INNER JOIN Users questioner ON question.OwnerUserId = questioner.Id

where 
question.PostTypeId=1 and 
question.Tags like '%<java>%' and
question.CreationDate >= '20100101' and 
question.CreationDate < '20100201'
GROUP BY question.Id