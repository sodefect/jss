import csv 

our_result_reader = csv.reader(open('/home/xeleb/projects/stackoverflow_post_challenge/result.csv','r'))
our_result_dictionary = {}
our_result_heading = next(our_result_reader)
our_result_heading.pop(0)
for row in our_result_reader:
    key = row[0]
    l = []
    l = row[1:len(row)]
    our_result_dictionary[key] = l

guru_result_reader = csv.reader(open('/home/xeleb/projects/stackoverflow_post_challenge/guru_result.csv','r'))
final_dictionary = {}
guru_result_headeing = next(guru_result_reader)

final_header = guru_result_headeing + our_result_heading

with open("final_result.csv",'w') as resultFile:
    wr = csv.writer(resultFile, dialect='excel')  
    wr.writerow(final_header)  
    for row in guru_result_reader:
        commit_hash = row[0]
        if commit_hash in our_result_dictionary.keys():
            our_list = our_result_dictionary[commit_hash]
            guru_list = row[1:len(row)]
            final_list = [commit_hash] + guru_list + our_list
            final_dictionary[commit_hash] = final_list
            wr.writerow(final_list)



    
