import calendar
import datetime
import re
import time

import pyautogui
import pyperclip
import requests
from selenium import webdriver


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)


def replace_last(source_string, replace_what, replace_with):
    head, _sep, tail = source_string.rpartition(replace_what)
    return head + replace_with + tail


def download_file(url, i):
    local_filename = 'month_{}.csv'.format(i)
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open('results/{}'.format(local_filename), 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename


base_url = 'https://data.stackexchange.com/account/login?returnurl=/'
base_date = datetime.date(2010, 2, 1)

query_raw = ''
inner = ''
with open('query.sql', 'r') as f:
    l = f.readline()
    while l:
        query_raw += l.strip()
        query_raw += ' '
        l = f.readline()

driver = webdriver.Firefox()
driver.get(base_url)
driver.find_elements_by_css_selector('div.preferred-login')[1].click()
email, password = driver.find_elements_by_css_selector('input.framed-text-field')
email.send_keys('mail@gmail.com')
password.send_keys('password')
driver.find_element_by_css_selector('input.affiliate-button').click()
driver.find_element_by_css_selector('a#compose-button').click()

for i in range(8 * 12 + 8):
    driver.find_element_by_css_selector('div.CodeMirror-code').click()
    pyautogui.hotkey('ctrl', 'a')
    pyautogui.hotkey('backspace')
    pyperclip.copy(query_raw)
    print(query_raw)
    start, end = re.findall(r'\d{8}', query_raw)
    last_date_begin = '%4d%02d%02d' % (base_date.year, base_date.month, base_date.day)
    base_date = add_months(base_date, 1)
    last_date_end = '%4d%02d%02d' % (base_date.year, base_date.month, base_date.day)
    query_raw = query_raw.replace(start, last_date_begin, 1)
    query_raw = replace_last(query_raw, end, last_date_end)
    pyautogui.hotkey('ctrl', 'v')

    driver.find_element_by_css_selector('button#submit-query').click()
    time.sleep(20)
    csv_link = driver.find_element_by_css_selector('a#resultSetsButton').get_attribute('href')
    download_file(csv_link, i)

# driver.execute_script("""
# (function(XHR) {
#   "use strict";
#
#   var element = document.createElement('div');
#   element.id = "interceptedResponse";
#   element.appendChild(document.createTextNode(""));
#   document.body.appendChild(element);
#
#   var open = XHR.prototype.open;
#   var send = XHR.prototype.send;
#
#   XHR.prototype.open = function(method, url, async, user, pass) {
#     this._url = url; // want to track the url requested
#     open.call(this, method, url, async, user, pass);
#   };
#
#   XHR.prototype.send = function(data) {
#     var self = this;
#     var oldOnReadyStateChange;
#     var url = this._url;
#
#     function onReadyStateChange() {
#       if(self.status === 200 && self.readyState == 4 /* complete */) {
#         document.getElementById("interceptedResponse").innerHTML +=
#           '{"data":' + self.responseText + '}*****';
#       }
#       if(oldOnReadyStateChange) {
#         oldOnReadyStateChange();
#       }
#     }
#
#     if(this.addEventListener) {
#       this.addEventListener("readystatechange", onReadyStateChange,
#         false);
#     } else {
#       oldOnReadyStateChange = this.onreadystatechange;
#       this.onreadystatechange = onReadyStateChange;
#     }
#     send.call(this, data);
#   }
# })(XMLHttpRequest);
# """)
# time.sleep(15)
#
# t = driver.find_element_by_css_selector('div#interceptedResponse').text
# t = t[t.index('{"data":{ "resultSets"'):]
# with open('xhr.txt', 'w') as x:
#     x.write(t)
#
# t = dict(json.loads(t))
# columns = t['data']['resultSets'][0]['columns']
# rows = t['data']['resultSets'][0]['rows']
# df = pd.DataFrame(rows, columns=columns)
# df.to_csv('m{}.csv'.format(i))


#########################################################################

# session = ''
# xauth = ''
# for cook in driver.get_cookies():
#     if cook['name'] == 'ASP.NET_SessionId':
#         session = cook['value']
#     if cook['name'] == '.ASPXAUTH':
#         xauth = cook['value']
#
# headers = {'Accept': '*/*',
#            'Accept-Encoding': 'gzip, deflate, br',
#            'Accept-Language': 'en-US,en;q=0.9,fa;q=0.8',
#            'Connection': 'keep-alive',
#            'Content-Length': '445',
#            'Content-Type': 'application/x-www-form-urlencoded',
#            'Cookie': 'ASP.NET_SessionId={}; .ASPXAUTH={}'.format(session, xauth),
#            'DNT': '1',
#            'Host': 'data.stackexchange.com',
#            'Origin': 'https://data.stackexchange.com',
#            'Referer': 'https://data.stackexchange.com/stackoverflow/query/new',
#            # 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0',
#            'X-Requested-With': 'XMLHttpRequest'
#            }
#
# query = "title=&description=&sql={}".format('select+question.Id+as+question_Id%2C+MAX%28question.CreationDate%29+as+question_CreationDate%2C+MAX%28question.Score%29+as+question_Score%2C+MAX%28question.ViewCount%29+as+question_ViewCount%2C+MAX%28question.AnswerCount%29+as+question_AnswerCount%2C+MAX%28question.CommentCount%29+as+question_CommentCount%2C+MAX%28question.FavoriteCount%29+as+question_FavoriteCount%2C+MAX%28question.ClosedDate%29+as+question_ClosedDate%2C+MAX%28accepted_answer.Id%29+as+accepted_answer_Id%2C+MAX%28accepted_answer.Score%29+as+accepted_answer_Score%2C+MAX%28all_answers.Score%29+as+max_score_answer%2C+SUM%28CASE+WHEN+question_votes.VoteTypeId+%3D+2+THEN+1+ELSE+0+END%29+as+question_upvotes%2C+SUM%28CASE+WHEN+question_votes.VoteTypeId+%3D+3+THEN+1+ELSE+0+END%29+as+question_downvotes%2C+MAX%28questioner.Reputation%29+as+questioner_reputation++from+Posts+question+LEFT+JOIN+Posts+accepted_answer+ON+question.AcceptedAnswerId+%3D+accepted_answer.Id+RIGHT+JOIN+Posts+all_answers+ON+all_answers.ParentId+%3D+question.Id+RIGHT+JOIN+Votes+question_votes+ON+question_votes.PostId+%3D+question.Id+INNER+JOIN+Users+questioner+ON+question.OwnerUserId+%3D+questioner.Id++where+question.PostTypeId%3D1+and+question.Tags+like+%27%25%3Cjava%3E%25%27+and+question.CreationDate+%3E%3D+%2720100101%27+and+question.CreationDate+%3C+%2720110201%27+GROUP+BY+question.Id')
#
# r1 = requests.post('https://data.stackexchange.com/query/save/1/', data=query, headers=headers)
# print(r1.headers)
# print(r1.text)
# print('###################################\n\n')
#
# time.sleep(3)
#
# headers2 = headers.copy()
# headers2['Accept'] = 'application/json, text/javascript, */*; q=0.01'
# headers2['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
# r2 = requests.post('https://data.stackexchange.com/mini-profiler-resources/results', data='id={}&clientPerformance=&clientProbes=&popup=1'.format(r1.headers['X-MiniProfiler-Ids'][0], headers=headers2))
# print(r2.headers)
# print(r2.text)
# print('###################################\n\n')
#
# time.sleep(3)
#
# name = r2.text['Root']['Name']
# _ = name[name.index('?_='):]
# print(_)
# print('###################################\n\n')
#
# r3 = requests.get('https://data.stackexchange.com/query/job/{}?_={}'.format(r1.text['job_id'], _), headers=headers)
# print(r3.headers)
# print(r3.text)
