
## Abstract

Software development nowadays is heavily based on libraries, frameworks and their proposed Application Programming Interfaces (APIs). However, due to challenges such as the complexity and the lack of documentation, these APIs may introduce various obstacles for developers and common defects in software systems. To resolve these issues, developers usually utilize Question and Answer (Q\&A) websites such as Stack Overflow by asking their questions and finding proper solutions for their problems on APIs. Therefore, these websites have become inevitable sources of knowledge for developers, which is also known as \textit{crowd knowledge}. 

However, the relation of this knowledge to the software quality has never been adequately explored before. In this paper, we study whether using APIs which are challenging according to the discussions of the Stack Overflow is related to code quality defined in terms of post-release defects. To this purpose, we define the concept of \textit{challenge of an API}, which denotes how much the API is discussed in high-quality posts on Stack Overflow. Then, using this concept, we propose a set of products and process metrics. We empirically study the statistical correlation between our metrics and post-release defects as well as added explanatory and predictive power to traditional models through a case study on five open source projects including Spring, Elastic Search, Jenkins, K-8 Mail Android Client, and OwnCloud Android client.

Our findings reveal that our metrics have a positive correlation with post-release defects which is comparable to known high-performance traditional process metrics, such as \textit{code churn} and \textit{number of pre-release defects}. Furthermore, our proposed metrics can provide additional explanatory  and predictive power for software quality when added to the models based on existing products and process metrics.

Our results suggest that, software developers should consider allocating more resources on reviewing and improving external API usages to prevent further defects.

# Architecture

You can find the code structure and the architecture of our analysis as follows:

![](code_structure.jpg)


# Plugin
To pave the way for using our approach and further studies on the use of crowd knowledge, we developed a plugin for IntelliJ IDEA - One of the most popular Java Integrated Development Environments (IDE) - which shows the challenge for the external APIs used in source code files.

## Installation

After you download the plugin archive (JAR), do the following:

    * In the Settings/Preferences dialog Ctrl+Alt+S, select Plugins.

    * On the Plugins page, click The Settings button and then click Install Plugin from Disk.

    * Select the plugin JAR file and click OK.

    * Click OK to apply the changes and restart the IDE if prompted.

## Manual

![](1.png)

1. After installation of plugin, a new icon appears at the top of IDE, near Run button.

2. By clicking the button, the plugin analyzes the current open source code file and lists APIs along with their corresponding challenge (in parentheses) in the bottom of the page inside Apis Challenging Output tab.

3. You can filter APIs with low challenge by setting a minimum challenging score.

4. To better provide insight, by clicking each item in the API Challenging Output tab, the plugin shows a list of Stack Overflow related questions in a new tab called Stack Overflow links.


![](2.png)




